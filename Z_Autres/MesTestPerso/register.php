<?php include('process.php'); ?>
<html>
<head>
    <title>Register</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
<form id="register_form">
    <h1>Register</h1>
    <div id="error_msg"></div>
    <div>
        <span></span>
        <input type="text" name="username" placeholder="Username" id="username"  oninput="username">

    </div>
    <div>
        <span></span>
        <input type="email" name="email" placeholder="Email" id="email">

    </div>
    <div>
        <input type="password" name="password" placeholder="Password" id="password">
    </div>
    <div>
        <button type="button" name="register" id="reg_btn">Register</button>
    </div>
</form>
</body>
</html>
<!-- scripts -->
<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.4.1.min.js"></script>
<script src="script.js"></script>
