<?php
$new_rows = array( '' => array(), 'Width' => array(), 'Material' => array(), 'Diameter' => array(), array());

foreach ($rows as $row) {
    $new_rows[''][] = array($row->image_url, $row->model_no);
    $new_rows['Width'][] = $row->width;
    $new_rows['Material'][] = $row->material;
    $new_rows['Diameter'][] = $row->diameter;
}

$similar_com = "<table>";

foreach ($new_rows as $key => $new_row) {

    if ($key == '') {
        $similar_com .= "<thead>";
    }

    $similar_com .= "<tr><th>" . $key . "</th>";
    foreach ($new_row as $inner_key => $item) {
        if ($key == '') {
            $similar_com .= "<th><img style='width:100px;height:100px' src='" . $item[0] . "' alt='" . $item[1] . "'>Product " . $inner_key . "</th>;
        }
        else {
            $similar_com .= "<td>" . $item;
            if ($key == "Width" || $key == "Diameter") {
                $similar_com .= " mm";
            }
            $similar_com .= "</td>
        }
    }

    $similar_com .= "</tr>";
    if ($key == '') {
        $similar_com .= "</thead>";
    }
}

$similar_com .= "</table>";

echo $similar_com;
