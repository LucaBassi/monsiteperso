-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le :  mar. 11 fév. 2020 à 19:27
-- Version du serveur :  5.6.40-84.0-log
-- Version de PHP :  5.5.38

-- SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `lab001_bd`
--

-- --------------------------------------------------------

--
-- Structure de la table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `autor` varchar(100) NOT NULL,
  `comment` varchar(200) NOT NULL,
  `post_id_comment` int(11) NOT NULL,
  `user_id_comment` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `comments`
--

INSERT INTO `comments` (`id`, `date`, `time`, `autor`, `comment`, `post_id_comment`, `user_id_comment`) VALUES
(1, '2020-01-31', '18:45:09', 'retest@mail.mail', 'hey salut !', 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `image`
--

CREATE TABLE `image` (
  `id` int(11) NOT NULL,
  `file` varchar(100) NOT NULL,
  `profil` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `image`
--

INSERT INTO `image` (`id`, `file`, `profil`, `username`) VALUES
(69, 'mozilla-firefox-wallpaper1.jpg', 'admin', 'admin'),
(70, '1574505323_mozilla-firefox-wallpaper.jpg', '', 'admin'),
(0, 'S153CM.jpg', 'luca', 'luca');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `user_type` varchar(20) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `user_type`, `password`) VALUES
(1, 'retest', 'retest@mail.mail', 'user', '$argon2i$v=19$m=1024,t=2,p=2$TllIZUNvcnJnM2NHbkhncw$TGjQPQ83SCGpHIrBilG7wMno7t2MDUqTUK/kwqFuiJE'),
(2, 'jg', 'test@s.v', '', '$2y$10$JGZ.0FvXFgM2fCnClN1mPOnVMcNqKsp4.L1pfvp478cH5qPjoK8Jm'),
(3, 'klj', 'test@s.v', '', '$2y$10$.ethmRWbYH9LyKkIZadpnupdCjs.Bkf/S1RW/BZnFXQbFxb0CttSK'),
(4, 'pd', 'pd_dylan@pd.mail', '', '$2y$10$q471UFPOKTwQtS00K4T5tuC2ahRnHXZFEk0laSad78nS0LCVxy2o6'),
(5, 'NULL', 'sdlk1260@gmail.com', '', '$2y$10$64QJA45ibyAHTz77PcEW/.VjDSYN7VOpJWwHzOJ7mTEybMAN47Aiq'),
(6, 'NULL', 'sdlk1260@gmail.com', '', '$2y$10$TLihtsN8GjrVYEqbmUkHR.4bFYsIIDeqg7Zl3EUPgK167J3dozkUu');

-- --------------------------------------------------------

--
-- Structure de la table `user_posts`
--

CREATE TABLE `user_posts` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `post` varchar(100) NOT NULL,
  `image` longblob NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `user_id_post` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `user_posts`
--

INSERT INTO `user_posts` (`id`, `name`, `post`, `image`, `date`, `time`, `user_id_post`) VALUES
(1, 'retest@mail.mail', 'salut', '', '2020-01-31', '20:03:40', 1);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_id` (`post_id_comment`),
  ADD KEY `user_id` (`user_id_comment`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `user_posts`
--
ALTER TABLE `user_posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id_post`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `user_posts`
--
ALTER TABLE `user_posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `post_id_comment` FOREIGN KEY (`post_id_comment`) REFERENCES `user_posts` (`id`),
  ADD CONSTRAINT `user_id_comment` FOREIGN KEY (`user_id_comment`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `user_posts`
--
ALTER TABLE `user_posts`
  ADD CONSTRAINT `user_id_post` FOREIGN KEY (`user_id_post`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
