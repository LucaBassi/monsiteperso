-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  sam. 01 fév. 2020 à 14:25
-- Version du serveur :  10.4.8-MariaDB
-- Version de PHP :  7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `multi_login`
--

-- --------------------------------------------------------

--
-- Structure de la table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL DEFAULT current_timestamp(),
  `autor` varchar(100) NOT NULL,
  `comment` varchar(200) NOT NULL,
  `post_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `comments`
--

INSERT INTO `comments` (`id`, `date`, `time`, `autor`, `comment`, `post_id`, `user_id`) VALUES
(0, '2020-01-29', '00:00:00', '', 'hhh', 3, 0),
(0, '2020-01-29', '00:00:00', '', 'super !\r\n', 3, 0),
(0, '2020-01-29', '00:00:00', '', 'encore !\r\n', 3, 0),
(0, '2020-01-29', '00:00:00', '', 'super ce post !', 5, 0),
(0, '2020-01-29', '00:00:00', '', 'w', 5, 0),
(0, '2020-01-29', '00:00:00', '', 'w', 5, 0),
(0, '2020-01-29', '00:00:00', 'test@mail.mail', 'ss', 1, 0),
(0, '2020-01-29', '00:00:00', 'test@mail.mail', 'super !', 1, 0),
(0, '2020-01-29', '00:00:00', 'test@mail.mail', 'ss', 1, 0),
(0, '2020-01-29', '00:00:00', 'test@mail.mail', 'bien joué', 1, 0),
(0, '2020-01-29', '00:00:00', 'test@mail.mail', 'zz', 1, 0),
(0, '2020-01-29', '00:00:00', 'test@mail.mail', 'yes !', 1, 0),
(0, '2020-01-30', '00:00:00', 'test@mail.mail', 'hey', 1, 0),
(0, '2020-01-30', '00:00:00', 'test@mail.mail', 'hey', 1, 0),
(0, '2020-01-30', '00:00:00', 'test@mail.mail', 's', 1, 0),
(0, '2020-01-30', '00:00:00', 'test@mail.mail', 'e', 1, 0),
(0, '2020-01-30', '00:00:00', 'test@mail.mail', 'h', 1, 0),
(0, '2020-01-30', '00:00:00', 'test@mail.mail', 'h', 1, 0),
(0, '2020-01-30', '00:00:00', 'test@mail.mail', 'hey', 1, 0),
(0, '2020-01-30', '00:20:20', 'retest@mail.mail', 'hey hey', 1, 0),
(0, '0000-00-00', '00:20:20', 'retest@mail.mail', 'ww', 1, 0),
(0, '0000-00-00', '00:20:20', 'retest@mail.mail', 'ww', 1, 0),
(0, '0000-00-00', '00:20:20', 'retest@mail.mail', 'ss', 1, 0),
(0, '0000-00-00', '00:20:20', 'retest@mail.mail', 'hh', 1, 0),
(0, '0000-00-00', '00:20:20', 'retest@mail.mail', 'hey', 1, 0),
(0, '0000-00-00', '00:20:20', 'retest@mail.mail', 'hey', 1, 0),
(0, '0000-00-00', '00:20:20', 'retest@mail.mail', 'hey', 1, 0),
(0, '2020-01-30', '19:54:20', 'retest@mail.mail', 're', 1, 0),
(0, '2020-01-31', '12:47:35', 'retest@mail.mail', 'bien joué', 1, 0),
(0, '2020-01-31', '13:05:41', 'retest@mail.mail', 'Ton post est super !', 1, 0),
(0, '2020-01-31', '13:10:25', 'retest@mail.mail', 'hey', 1, 0),
(0, '2020-01-31', '13:14:48', 'retest@mail.mail', '', 3, 0),
(0, '2020-01-31', '13:15:02', 'retest@mail.mail', 'hey', 3, 0),
(0, '2020-01-31', '13:17:30', 'retest@mail.mail', 'yy', 3, 0),
(0, '2020-01-31', '13:22:27', 'retest@mail.mail', '', 3, 0),
(0, '2020-01-31', '15:24:34', 'retest@mail.mail', 'il est 15 24', 3, 0),
(0, '2020-01-31', '15:25:43', 'retest@mail.mail', 'aa', 3, 0),
(0, '2020-01-31', '15:26:41', 'retest@mail.mail', 'ss', 3, 0),
(0, '2020-01-31', '15:30:39', 'retest@mail.mail', 'qq', 2, 0),
(0, '2020-01-31', '15:30:54', 'retest@mail.mail', 'dd', 2, 0),
(0, '2020-01-31', '15:32:03', 'retest@mail.mail', 'dd', 2, 0),
(0, '2020-01-31', '15:32:33', 'retest@mail.mail', 'ee', 2, 0),
(0, '2020-01-31', '15:33:09', 'retest@mail.mail', 'ff', 2, 0),
(0, '2020-01-31', '15:33:31', 'retest@mail.mail', 'hey', 2, 0),
(0, '2020-01-31', '17:32:08', 'retest@mail.mail', 'dd', 1, 0),
(0, '2020-01-31', '18:35:34', 'retest@mail.mail', 'ddd', 1, 0),
(0, '2020-01-31', '18:38:12', 'retest@mail.mail', 'ss', 1, 0),
(0, '2020-01-31', '18:45:09', 'retest@mail.mail', 'hey', 1, 0),
(0, '2020-01-31', '18:47:18', 'retest@mail.mail', 'hey', 1, 0);

-- --------------------------------------------------------

--
-- Structure de la table `image`
--

CREATE TABLE `image` (
  `id` int(11) NOT NULL,
  `file` varchar(100) NOT NULL,
  `profil` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `image`
--

INSERT INTO `image` (`id`, `file`, `profil`, `username`) VALUES
(69, 'mozilla-firefox-wallpaper1.jpg', 'admin', 'admin'),
(70, '1574505323_mozilla-firefox-wallpaper.jpg', '', 'admin'),
(0, 'S153CM.jpg', 'luca', 'luca');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `user_type` varchar(20) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `user_type`, `password`) VALUES
(1, 'luca2', 'l.bassi@hotmail.fr', 'admin', 'ff377aff39a9345a9cca803fb5c5c081'),
(2, 'luca@mail.mail', '', 'admin', '$2y$10$RUjjia8tXqq7mOK45XcoHOoJY9ML8wqUDwOKR5hjkkeAfxzD3jJ1G'),
(3, 'luca@kjbghk.kjhkh', '', '', ''),
(4, 'kj@jhkj.kljbklj', '', '', '$argon2i$v=19$m=131072,t=4,p=2$S3p0aXcuUDFobFBPVWh0Nw$QXWvQGF+yxzo+KGU5Xcdpr3f0zqmI/sLKg8bqth0Rxc'),
(5, 'ihb@bjhb.shg', '', '', ''),
(6, 'hgvhv@kjbkb.ékjhh', '', '', ''),
(7, 'ljlhj@kj.xkh', '', '', ''),
(8, 'kb@j.h', '', '', '$2y$10$8rLSztqTvX6zCXoN1wlWNeoS0FETqtHv0xVId7Du5nGAMmzGQLKnC'),
(9, 'JKG@KLJHKJ.HH', '', '', ''),
(10, 'IHKUH@KB.KJ', '', '', ''),
(11, 'JHVG@KJB.FR', '', '', ''),
(12, 'JJJ@JJJ.HH', '', '', ''),
(13, 'hhh@kk.h', '', '', ''),
(14, 'ugg@khg.c', '', '', ''),
(15, 'salu@salul.d', '', '', '$argon2i$v=19$m=1024,t=2,p=2$bTlMMGxQQjRWcWdWb09Oaw$LAKVlD4myAggJ08W+lomNpYAEOvGlfVKpxkQ6/ngN3w'),
(16, 'test@mail.mail', '', '', '$argon2i$v=19$m=1024,t=2,p=2$Y2VnZXRkdzE5dVdaNHVoQg$wJw5zZ5lIp22Ee0PsRc5Z2VaDqTNUNvFCcvFGDWWTIw'),
(17, 'datch', 'luca@mail.mail2', 'user', '$argon2i$v=19$m=1024,t=2,p=2$L29NbGd5UlhET2J5cWMwaw$OqKn8c7/ZG8zev4Bp76V2EFvgmkRJ0a4o9rTWsM0InI'),
(20, 'retest', 'retest@mail.mail', 'user', '$argon2i$v=19$m=1024,t=2,p=2$TllIZUNvcnJnM2NHbkhncw$TGjQPQ83SCGpHIrBilG7wMno7t2MDUqTUK/kwqFuiJE'),
(21, 'salut', 'salut@salut.salu', '', '$argon2i$v=19$m=1024,t=2,p=2$T1pHdUpCdU81ZkVrVDVDcw$/orWv9LmhWcBaV6WUVM/hee03djXbyLRRNz+Ev+v9IM');

-- --------------------------------------------------------

--
-- Structure de la table `user_posts`
--

CREATE TABLE `user_posts` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `post` varchar(100) NOT NULL,
  `date` date NOT NULL DEFAULT current_timestamp(),
  `time` time NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `user_posts`
--

INSERT INTO `user_posts` (`id`, `name`, `post`, `date`, `time`) VALUES
(1, 'luca@mail.mail', 'yes', '2020-01-30', '19:12:46'),
(2, 'luca@mail.mail', 'bla bla', '2020-01-30', '19:12:46'),
(3, 'luca@mail.mail', 'youpi', '2020-01-30', '19:12:46'),
(4, 'salu@salul.d', 'youyou', '2020-01-30', '19:12:46'),
(5, 'test@mail.mail', 'heeeey2', '2020-01-30', '19:12:46'),
(6, 'retest@mail.mail', 'hey !!', '2020-01-31', '19:50:14'),
(7, 'retest@mail.mail', 'hey !!', '2020-01-31', '19:51:15'),
(8, 'retest@mail.mail', 'hey !!', '2020-01-31', '19:52:05'),
(9, 'retest@mail.mail', 'hey !!', '2020-01-31', '19:52:54'),
(10, 'retest@mail.mail', 'hey !!', '2020-01-31', '19:53:38'),
(11, 'retest@mail.mail', 'hey !!', '2020-01-31', '19:54:08'),
(12, 'retest@mail.mail', 'hey !!', '2020-01-31', '19:54:32'),
(13, 'retest@mail.mail', 'hey !!', '2020-01-31', '19:54:42'),
(14, 'retest@mail.mail', 'hey !!', '2020-01-31', '19:55:04'),
(15, 'retest@mail.mail', 'hey !!', '2020-01-31', '19:55:29'),
(16, 'retest@mail.mail', 'hey !!', '2020-01-31', '19:56:19'),
(17, 'retest@mail.mail', 'hey !!', '2020-01-31', '19:56:49'),
(18, 'retest@mail.mail', 'hey !!', '2020-01-31', '19:57:02'),
(19, 'retest@mail.mail', 'hey !!', '2020-01-31', '19:57:10'),
(20, 'retest@mail.mail', 'hey !!', '2020-01-31', '19:57:48'),
(21, 'retest@mail.mail', 'hey !!', '2020-01-31', '19:58:06'),
(22, 'retest@mail.mail', 'hey !!', '2020-01-31', '19:58:39'),
(23, 'retest@mail.mail', 'hey !!', '2020-01-31', '19:58:50'),
(24, 'retest@mail.mail', 'hey !!', '2020-01-31', '19:59:20'),
(25, 'retest@mail.mail', 'hey !!', '2020-01-31', '19:59:38'),
(26, 'retest@mail.mail', 'hey !!', '2020-01-31', '20:00:14'),
(27, 'retest@mail.mail', 'hey !!', '2020-01-31', '20:00:28'),
(28, 'retest@mail.mail', 'hey !!', '2020-01-31', '20:01:33'),
(29, 'retest@mail.mail', 'hey !!', '2020-01-31', '20:01:56'),
(30, 'retest@mail.mail', 'hey !!', '2020-01-31', '20:02:29'),
(31, 'retest@mail.mail', 'hey !!', '2020-01-31', '20:02:43'),
(32, 'retest@mail.mail', 'salut', '2020-01-31', '20:03:32'),
(33, 'retest@mail.mail', 'salut', '2020-01-31', '20:03:40'),
(34, 'retest@mail.mail', 'salut', '2020-02-01', '08:50:56');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `comments`
--
ALTER TABLE `comments`
  ADD KEY `user_id` (`user_id`),
  ADD KEY `post_id` (`post_id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `user_posts`
--
ALTER TABLE `user_posts`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT pour la table `user_posts`
--
ALTER TABLE `user_posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `post_id` FOREIGN KEY (`post_id`) REFERENCES `user_posts` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
