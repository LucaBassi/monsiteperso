<?php


function getBdd()
{
    $bdd = new PDO('mysql:host=localhost;dbname=mydemo', 'root', '', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
    return $bdd;

}



function queryBuilder($params)
{

    foreach ($params as $key => &$val) {
        $stmt->bindParam($key, $val);
        $loginQuery = "SELECT password FROM users WHERE email = :email ";
        $stmt->bindValues($stmt);
        executeQuerySelect($loginQuery);

    }
}


function executeQuerySelect($query, $params)
{
    $queryResult = null;


    $dbConnexion = openDBConnexion();//open database connexion
    if ($dbConnexion != null) {

        $statement = $dbConnexion->prepare("$query");//prepare query
        foreach ($params as $key => &$val) {
           // $statement->bindParam($key, $val, \PDO::PARAM_STR);
            $statement->bindValue("$key", "$val", \PDO::PARAM_STR);
        }

        $statement->execute();//execute query
        $queryResult = $statement->fetchAll();//prepare result for client
    }
    $dbConnexion = null;//close database connexion
    return $queryResult;
}
/**
 * This function is designed to insert value in database
 * @param $query
 * @return bool|null : $statement->execute() returne true is the insert was successful
 */


function executeQueryInsert($query)
{
    $queryResult = null;

    $dbConnexion = openDBConnexion();//open database connexion
    if ($dbConnexion != null) {
        $statement = $dbConnexion->prepare($query);//prepare query
        $queryResult = $statement->execute();//execute query
    }
    $dbConnexion = null;//close database connexion
    return $queryResult;
}

function openDBConnexion()
{
    $tempDbConnexion = null;

    $sqlDriver = 'mysql';
    $hostname = 'localhost';
    $port = 3306;
    $charset = 'utf8';
    $dbName = 'mydemo';
    $userName = 'root';
    $userPwd = '';
    $dsn = $sqlDriver . ':host=' . $hostname . ';dbname=' . $dbName . ';port=' . $port . ';charset=' . $charset;

    try {
        $tempDbConnexion = new PDO($dsn, $userName, $userPwd);
    } catch (PDOException $exception) {
        echo 'Connection failed: ' . $exception->getMessage();
    }
    return $tempDbConnexion;
}
